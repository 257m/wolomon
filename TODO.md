# TODO

## Future Tasks
- Entity Management system
	- Wolomon hostile, wild and tamed mobs
- Chunk Management System
	- Chunks need to be able to get deleted after they are unused for a while.
	- Chunks need to still be cached in the local area in case player doubles back.  
	  also chunks need to be loaded ahead of time in the local area to reduce having  
	  to load massive amounts of chunks in a short time span.
	- Chunks need to be able to be saved to disk and easily retrieved

## In Progress
- Entity Management system | Estimated progress: 25%  
	- Wolomon spawning | Estimated progress: 30%  
		Every frame wolomon will try to spawn in a random location within the world border.  
		If the amount of entities in the world is at its maximum the spawn will fail and it  
		will try again next time to see if anything has been despawned that it can replace.
		Next steps:  
			- I need to make the spawn logic wolomon specific and more realistic than entities  
			  spawning in every frame to fill up the max entity count.  
	- Wolomon despawning | Estimated progress: 20%  
		Every frame there will be a check as to whether the entity is outside the world border.  
		If it ends up outside the world border it gets deleted from the game.  
		Next steps:  
			Will need to handle things like entities dying and what not.  
	- Movement AI | Estimated progress: 2%  
		The movement AI is currently is very rudimentary. Every 5 seconds an entity will decide  
		to change its desired velocity and the game will apply a random force to it. I currently  
		have the entity rotating so that it always faces its velocity but this will need to change soon.  
		By the end of it, I will need path finding and complex movement patterns.  
	Next steps:  
		Having the entity system better interact with the rest of the game is a must.  
		I might end up changing the queue structure of how it is managing subsequence  
		additions and deletions by having the queue actually be for only ids that reference  
		the real places where the entities are stored. This so that entity have a constant  
		place in memory over their entire lifetimes relative to the array that is managing them  
		In this way, when a process needs an entity, an empty slot can be obtained via tail index  
		of the queue and when an entity is deleted the entity replacing it in its buffer needs not  
		care about who is referencing it because its id relative to the array is constant. For entity  
		references I will never be storing entities pointers as this is dangerous and instead having  
		static ids make things much simpler. Going back to interactions with the world I want the player  
		being able to fight entities in a crude way would a good first step.  
		
- Model renderer | Estimated progress: 40%  
	The model renderer can render arbitrary models with vertices and uvs in a custom .s3m format.  
	The model renderer will probably need to handle lighting using normals and also having multiple  
	different objects inside of a model so later I can do animations where I move the individual objects.
	Animations are going to be a big workload and so I first want to get a couple of different entity models  
	into the game before committing to designing the system.


## Finished Tasks
- Basic Polygonal terrain mesh renderer
- Crude Physics
