# INSTALLATION INSTRUCTIONS

# Mac / OSX
For Mac you will need a couple of pre requisites to be able to setup up a dev environment and run the code. (fewer steps than Windows)  
Since I don't have a Mac myself I wouldn't be shipping my own binaries so its going to be a full development environment where you compile the code  
and can edit it yourself. The first step is to install homebrew.

### Installing Homebrew
Homebrew is package manager for Mac OS that makes installing software very easy. https://brew.sh/  
Here is a video to guide you through the install if you need it: https://youtu.be/l3bh70tyhe8  
The reason you need homebrew is so that you can easily install the tools needed without having to deal with the tragedy that is xcode directly.  
First you need to pull down a shell script and run it. To do this paste the command below in the Mac OS terminal.  

`/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"`

This will run a script to guide you through the install and probably ask a couple of questions of how you want things installed.
If it asks to install xcode just hit enter.

Once that is done, it should be installed and you only have one step left. You need to add brew to your $PATH variable so that the OS is aware of it.  
This two commands will do the trick:

`(echo; echo 'eval "\$(/opt/homebrew/bin/brew shellenv)"') >> /Users/$USER/.zprofile`
`eval "\$(/opt/homebrew/bin/brew shellenv)"`

Although it might look a bit different for you and it will tell that you need to run these two commands at the end of the script  
and it is better just to run the ones it gives you rather than these ones.

### Installing the dependencies for the project
In my projects I try to minimize the dependencies to run and change it as much as possible but alas there are always *some* dependencies.  
For the project I use glfw and opengl as external libraries that I link against and git, gcc, make and pkg-config as development.  
Opengl will come with your system and your graphics drivers so we don't need to worry about that.  
So lets go ahead and install the things we need using brew:

`brew install glfw git gcc make pkg-config`

### Actually getting the project running
Now that we have all the software we need, we actually need to clone the repo and compile the code.  
I have condensed all the steps needed into a single command using `&&` but feel free to individually paste if you need it.  
It might also be a good idea to make a proper directory location for this project so just type `mkdir ~/projects/ && cd ~/projects` and click enter.

Now run the command below and it will download, install/compile and run the program in a bit of time.  
If it throws an error please report back to me with the error message so I can fix it.

`git clone https://codeberg.org/257m/wolomon && cd wolomon && make clean && make && ./main`

This clones my repo, moves into it, clears the cache of object files, recompiles everything and then runs the program.  
If you want to run the program again. Make sure you are in the proper directory (~/projects/wolomon) and type `./main` to run it.  
You can use the change directory command `cd` to move to the directory everytime you need to rerun it or just open the folder in a file manager and double click on `main`  
For example `cd ~/projects/wolomon/` to move to the directory and `./main` to run it.

Later I will post more documentation on how to edit the code, push your changes to the main repo and ultimately how to actually work on the project in a sane environment  
but it is getting late and I have school tomorrow.
