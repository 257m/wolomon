#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <signal.h>
#include "model.h"
#include "errors.h"

typedef enum {
	TOK_EOF,
	TOK_VERTEX,
	TOK_NORMAL,
	TOK_TEXCOORD,
	TOK_FACE,
	TOK_ERROR,
} Model_Token;

static Model_Token last_tok = TOK_ERROR;
static Model_Token cur_tok = TOK_ERROR;

// Going to have to be basic for now
static Model_Token get_tok(FILE* f)
{
	static Model_Token last_char = ' ';
redo_token:
	while (isspace(last_char))
		last_char = fgetc(f);
	if (last_char == 'v') {
		last_char = ' ';
		switch (fgetc(f)) {
			case ' ':
				return TOK_VERTEX;
			case 'n':
				return TOK_NORMAL;
			case 't':
				return TOK_TEXCOORD;
			default:
				return TOK_ERROR;
		}
	}

	if (last_char == 'f') {
		last_char = fgetc(f);
		return TOK_FACE;
	}
	// We are only supporting 3 syntactical structures for now
	// We pretend every thing else is just a comment as we do not need it
	while ((last_char = fgetc(f)) != EOF)
		if (last_char == '\n')
			goto redo_token;
	return TOK_EOF;
}

#define get_next_tok() ({last_tok = cur_tok; cur_tok = get_tok(f);})

Model* model_load(FILE* f)
{
	Model* m = malloc(sizeof(Model));
	// These can be shrunk later. Default expected size of model is 256 vertices
	m->vertex.vecs = malloc(sizeof(f32)*3*256);
	m->vertex.amount = 0;
	m->vertex.allocated = 3*256;
	m->vertex_indices.indices = malloc(sizeof(u32)*12*128);
	m->vertex_indices.amount = 0;
	m->vertex_indices.allocated = 12*128;

	// 1 normal per 3 vertices
	m->normal.vecs = malloc(sizeof(f32)*256);
	m->normal.amount = 0;
	m->normal.allocated = 256;
	m->normal_indices.indices = malloc(sizeof(u32)*12*128);
	m->normal_indices.amount = 0;
	m->normal_indices.allocated = 12*128;

	m->texcoord.vecs = malloc(sizeof(f32)*2*256);
	m->texcoord.amount = 0;
	m->texcoord.allocated = 2*256;
	m->texcoord_indices.indices = malloc(sizeof(u32)*12*128);
	m->texcoord_indices.amount = 0;
	m->texcoord_indices.allocated = 12*128;

	while (1)
		switch (get_next_tok()) {
			case TOK_VERTEX:
					if (m->vertex.amount+3 >= m->vertex.allocated)
						m->vertex.vecs = realloc(m->vertex.vecs, (m->vertex.allocated *= 4)*sizeof(f32));
					fscanf(f, "%f %f %f", &m->vertex.vecs[m->vertex.amount], &m->vertex.vecs[m->vertex.amount+1], &m->vertex.vecs[m->vertex.amount+2]);
					m->vertex.amount += 3;
					//printf("Vertex: %f %f %f\n", m->vertex.vecs[m->vertex.amount-1], m->vertex.vecs[m->vertex.amount-2], m->vertex.vecs[m->vertex.amount-3]);
					break;
			case TOK_NORMAL:
					if (m->normal.amount+3 >= m->normal.allocated)
						m->normal.vecs = realloc(m->normal.vecs, (m->normal.allocated *= 4)*sizeof(f32));
					fscanf(f, "%f %f %f", &m->normal.vecs[m->normal.amount], &m->normal.vecs[m->normal.amount+1], &m->normal.vecs[m->normal.amount+2]);
					m->normal.amount += 3;
					break;
			case TOK_TEXCOORD:
					if (m->texcoord.amount+2 >= m->texcoord.allocated)
						m->texcoord.vecs = realloc(m->texcoord.vecs, (m->texcoord.allocated *= 4)*sizeof(f32));
					fscanf(f, "%f %f", &m->texcoord.vecs[m->texcoord.amount], &m->texcoord.vecs[m->texcoord.amount+1]);
					m->texcoord.amount += 2;
					break;
			case TOK_FACE:
					for (u8 i = 0; i < 3; i++) {
						if (m->indices[i].amount+4 >= m->indices[i].allocated)
							m->indices[i].indices = realloc(m->indices[i].indices, (m->indices[i].allocated *= 4)*sizeof(u32));
						m->indices[i].amount += 3;
					}
					if (!m->indices[0].indices)
						raise(SIGABRT);
					fscanf(f, "%u/%u/%u %u/%u/%u %u/%u/%u",
					          &m->indices[0].indices[m->indices[0].amount-3],&m->indices[1].indices[m->indices[1].amount-3],&m->indices[2].indices[m->indices[2].amount-3],
					          &m->indices[0].indices[m->indices[0].amount-2],&m->indices[1].indices[m->indices[1].amount-2],&m->indices[2].indices[m->indices[2].amount-2],
					          &m->indices[0].indices[m->indices[0].amount-1],&m->indices[1].indices[m->indices[1].amount-1],&m->indices[2].indices[m->indices[2].amount-1]
					);
					/*printf("%u/%u/%u %u/%u/%u %u/%u/%u\n",
					       m->indices[0].indices[m->indices[0].amount-3],m->indices[1].indices[m->indices[1].amount-3],m->indices[2].indices[m->indices[2].amount-3],
					       m->indices[0].indices[m->indices[0].amount-2],m->indices[1].indices[m->indices[1].amount-2],m->indices[2].indices[m->indices[2].amount-2],
					       m->indices[0].indices[m->indices[0].amount-1],m->indices[1].indices[m->indices[1].amount-1],m->indices[2].indices[m->indices[2].amount-1]
					);*/
					break;
			case TOK_EOF:
				// If I have memory issues I can shrink the models down to the minimum
				/* m->vertex.vecs = realloc(m->vertex.vecs, m->vertex.amount*sizeof(f32));
				m->vertex.allocated = m->vertex.amount;
				m->normal.vecs = realloc(m->normal.vecs, m->normal.amount*sizeof(f32));
				m->normal.allocated = m->normal.amount;
				m->texcoord.vecs = realloc(m->texcoord.vecs, m->texcoord.amount*sizeof(f32));
				m->texcoord.allocated = m->texcoord.amount; */
				return m;
			default:
				raise(SIGABRT);
		}
}
