#ifndef ERRORS_H
#define ERRORS_H

#include <stdio.h>
#include <signal.h>

#define PANIC(ERROR, FORMAT, ...)                                                   \
	if (ERROR) {                                                                 	\
		fprintf(stderr, "%s -> %s -> %i:\n\t" FORMAT, __FILE_NAME__,                \
						__FUNCTION__, __LINE__, ##__VA_ARGS__);                     \
		raise(SIGABRT);                                                          	\
	}                                                                            	\

#endif /* ERRORS_H */
