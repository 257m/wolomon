#include <stdio.h>
#include <string.h>
#include "errors.h"
#include "model.h"

enum Options {
    PLAIN_VBO = 0,
    IBO = 1,
};

typedef struct {
    u8 vbo : 1;
} Options;

Options op = {PLAIN_VBO};

int main(int argc, char** argv)
{
    if (argc < 3) {
        printf("usage %s [options] input_model output_model\r\n", argv[0]);
        printf("-vbo option -> Uses plain vbos.\r\n"
                "-ibo option -> Uses IBOs for rendering\r\n"
              );
        raise(SIGABRT);
    }
    u32 option_offset = 0;
    for (u8 i = 1; i < argc; i++)
        if (argv[i][0] == '-') {
            if (strcmp(argv[i], "-ibo") == 0)
                op.vbo = IBO;
            else if (strcmp(argv[i], "-vbo") == 0)
                op.vbo = PLAIN_VBO;
            else {
                printf("Invalid option!\r\n");
                raise(SIGABRT);
            }
            option_offset++;
        }
	FILE* input_file = fopen(argv[option_offset+1], "r");
	PANIC(!input_file, "Failed to open model file %s\r\n", argv[1]);
    Model* m = model_load(input_file);
    FILE* output_file = fopen(argv[option_offset+2], "w");
    PANIC(!output_file, "Failed to create output model file %s\r\n", argv[2]);
    char header [80] = "S3M File. This file was generated from modconv";
    fwrite(header, 1, sizeof(header), output_file);
    if (op.vbo == PLAIN_VBO) {
        printf("Original Amount: %d,%d,%d\n", m->floats[0].amount, m->floats[1].amount, m->floats[2].amount);
        printf("Final Amount: %d,%d,%d\n", m->indices[0].amount, m->indices[1].amount, m->indices[2].amount);
        for (u32 i = 0; i < 3; i++)
            fwrite(&m->indices[i].amount, sizeof(m->indices[i].amount), 1, output_file);
        // There is always more vertices than there are normals or texcoords so this is safe
        for (u32 n = 0; n < m->vertex_indices.amount; n++) {
            fwrite(&m->vertex.vecs[(m->vertex_indices.indices[n]-1)*3], sizeof(f32), 3, output_file);
            fwrite(&m->texcoord.vecs[(m->texcoord_indices.indices[n]-1)*2], sizeof(f32), 2, output_file);
            fwrite(&m->normal.vecs[(m->normal_indices.indices[n]-1)*3], sizeof(f32), 3, output_file);
        }
    }
    else {
        /*bool* index_map = malloc(sizeof(bool)*m->vertex.amount);
        for (u32 n = 0; n < m->indices->amount; n++) {
            // Is it already in my vertex list?
            if (index_map[m->vertex_indices.indices[n]]) {
                
            }
            else {
                index_map[n] = 
            }

        }*/
    }
}
