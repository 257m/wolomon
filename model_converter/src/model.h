#ifndef MODEL_H
#define MODEL_H

#include "data_types.h"
#include <stdio.h>

typedef struct {
	f32* vecs;
	u32 amount;
	u32 allocated;
} Dynamic_Float_Array;

typedef struct {
	u32* indices;
	u32 amount;
	u32 allocated;
} Dynamic_UInt_Array;

typedef struct {
	union {
		Dynamic_Float_Array floats [3];
		struct {
			Dynamic_Float_Array vertex, texcoord, normal;
		};
	};
	union {
		Dynamic_UInt_Array indices [3];
		struct {
			Dynamic_UInt_Array vertex_indices, texcoord_indices, normal_indices;
		};
	};
} Model;

Model* model_load(FILE* f);

#endif /* MODEL_H */
