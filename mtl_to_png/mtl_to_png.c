#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <string.h>

#define IMAGE_WIDTH 512
#define IMAGE_HEIGHT 512

typedef struct {
    float r;
    float g;
    float b;
} Color;

typedef struct {
    Color ambient_color;
    Color diffuse_color;
    Color specular_color;
    float specular_exponent;
    float transparency;
} Material;

// Function to parse .mtl file and extract material properties
Material parse_mtl(const char* filename) {
    FILE* file = fopen(filename, "r");
    if (!file) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    char line[256];
    Material material = {{0, 0, 0}, {1, 1, 1}, {1, 1, 1}, 0.0, 1.0};  // Default values

    while (fgets(line, sizeof(line), file)) {
        if (strstr(line, "Ka")) {
            sscanf(line, "Ka %f %f %f", &material.ambient_color.r, &material.ambient_color.g, &material.ambient_color.b);
        } else if (strstr(line, "Kd")) {
            sscanf(line, "Kd %f %f %f", &material.diffuse_color.r, &material.diffuse_color.g, &material.diffuse_color.b);
        } else if (strstr(line, "Ks")) {
            sscanf(line, "Ks %f %f %f", &material.specular_color.r, &material.specular_color.g, &material.specular_color.b);
        } else if (strstr(line, "Ns")) {
            sscanf(line, "Ns %f", &material.specular_exponent);
        } else if (strstr(line, "d")) {
            sscanf(line, "d %f", &material.transparency);
        }
    }

    fclose(file);
    return material;
}

// Function to generate texture image
void generate_png(const char* filename, Material material) {
    uint8_t image[IMAGE_WIDTH * IMAGE_HEIGHT * 3];  // RGB image buffer

    // Calculate color step for gradient
    float step_r = 255*(material.diffuse_color.r - material.ambient_color.r) / IMAGE_WIDTH;
    float step_g = 255*(material.diffuse_color.g - material.ambient_color.g) / IMAGE_WIDTH;
    float step_b = 255*(material.diffuse_color.b - material.ambient_color.b) / IMAGE_WIDTH;

    // Fill image buffer with gradient
    for (int y = 0; y < IMAGE_HEIGHT; y++) {
        for (int x = 0; x < IMAGE_WIDTH; x++) {
            // Calculate color at current position
            int r = 255*(material.ambient_color.r) + (step_r * x);
            int g = 255*(material.ambient_color.g) + (step_g * x);
            int b = 255*(material.ambient_color.b) + (step_b * x);
            // Set pixel color in image buffer
            image[(y * IMAGE_WIDTH + x) * 3] = r;
            image[(y * IMAGE_WIDTH + x) * 3 + 1] = g;
            image[(y * IMAGE_WIDTH + x) * 3 + 2] = b;
        }
    }

    // Write image buffer to file
    FILE* file = fopen(filename, "wb");
    if (!file) {
        perror("Error opening file");
        exit(EXIT_FAILURE);
    }

    fprintf(file, "P6\n%d %d\n255\n", IMAGE_WIDTH, IMAGE_HEIGHT);
    fwrite(image, sizeof(uint8_t), IMAGE_WIDTH * IMAGE_HEIGHT * 3, file);

    fclose(file);
}

int main(int argc, char* argv[]) {
    if (argc != 3) {
        fprintf(stderr, "Usage: %s <input_mtl_file> <output_png_file>\n", argv[0]);
        return EXIT_FAILURE;
    }

    const char* input_mtl_file = argv[1];
    const char* output_png_file = argv[2];

    // Parse .mtl file and extract material properties
    Material material = parse_mtl(input_mtl_file);

    // Generate .png image with extracted material properties
    generate_png(output_png_file, material);

    printf("Generated %s\n", output_png_file);
    return EXIT_SUCCESS;
}
