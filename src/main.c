#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <glad/gl.h>
#include <GLFW/glfw3.h>
#include "cglm/types-struct.h"
#include "utils/cstr.h"
#include "utils/data_types.h"
#include "entity.h"
#include "model.h"
#include "state.h"
#include "utils/errors.h"
#include "text.h"

State state;

void cursor_position_callback(GLFWwindow* window, double xpos, double ypos)
{
	static const float mousespeed = 0.0001f;

	state.xpos = xpos;
	state.ypos = ypos;

	state.world.angle.x -= (xpos - state.window_width / 2) * mousespeed;
	state.world.angle.y -= (ypos - state.window_height / 2) * mousespeed;
	if (state.world.angle.x < -M_PI)
		state.world.angle.x += M_PI * 2;
	if (state.world.angle.x > M_PI)
		state.world.angle.x -= M_PI * 2;
	if (state.world.angle.y < -M_PI / 2)
		state.world.angle.y = -M_PI / 2;
	if (state.world.angle.y > M_PI / 2)
		state.world.angle.y = M_PI / 2;

	world_vector_update(&state.world, xpos, ypos);
	glfwSetCursorPos(state.window, state.window_width / 2, state.window_height / 2);
}

int main()
{
	srand(time(NULL));
	state.window_width = 1920;
	state.window_height = 1080;
	state.xpos = 0;
	state.ypos = 0;
	state.event_queue = event_queue_create(1024);

	PANIC(!glfwInit(), "Failed to initialize GLFW\n");
	glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing

	// Open a window and create its OpenGL context
	state.window = glfwCreateWindow(state.window_width, state.window_height, "Wolomon", glfwGetPrimaryMonitor(), NULL);

	PANIC(!state.window, "Failed to open GLFW window.\n");

	glfwMakeContextCurrent(state.window);
	glfwSetKeyCallback(state.window, event_queue_key_callback);
	glfwSetCursorPosCallback(state.window, cursor_position_callback);
	glfwSetInputMode(state.window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

	// Get info of GPU and supported OpenGL version
	/*printf("Renderer: %s\n", glGetString(GL_RENDERER));
	  printf("OpenGL version supported %s\n", glGetString(GL_VERSION));*/
	glfwSetCursorPos(state.window, state.window_width / 2, state.window_height / 2);
	int version = gladLoadGL(glfwGetProcAddress);

	world_init(&state.world, 20);
	model_init();
	models_init();
	text_init();
	em_init(&state.em, 1000, 1000);
	Event e;
	f64 current_time = 0, last_time = 0;
	while (!glfwWindowShouldClose(state.window)) {
		// Render the current frame
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		world_render(&state.world);
		em_add_entity(&state.em, &state.world);
		em_render(&state.em, &state.world);
		string pos = string_size(256);
		pos.len = snprintf(pos.data, pos.allocated, "%f, %f, %f", state.world.position.x, state.world.position.y, state.world.position.z);
		text_render(pos, -0.5, -1, vec3s(0.0, 0.0, 0.0));
		glfwSwapBuffers(state.window);

		// Deal with events
		glfwPollEvents();
		current_time = glfwGetTime();
		f64 dt = current_time - last_time;
		last_time = current_time;
		const f64 default_movespeed = 50;
		const f64 jumpspeed = 50;
		while (event_queue_poll(state.event_queue, &e)) {
			if (e.action == GLFW_PRESS)
				switch(e.key) {
					case GLFW_KEY_ESCAPE:
						glfwSetWindowShouldClose(state.window, GL_TRUE);
						break;
					case GLFW_KEY_PAGE_UP:
					case GLFW_KEY_SPACE:
						state.world.velocity.y = jumpspeed;
						break;
					case GLFW_KEY_PAGE_DOWN:
						state.world.velocity.y = -jumpspeed;
					default:
						break;
				}
		}
		f32 yvel = state.world.velocity.y;
		f64 movespeed = default_movespeed;
		f64 player_height = 2;
		if (glfwGetKey(state.window, GLFW_KEY_C)) {
			movespeed = default_movespeed/10;
			player_height = 1;
		}
		if (glfwGetKey(state.window, GLFW_KEY_W) == GLFW_PRESS || glfwGetKey(state.window, GLFW_KEY_UP) == GLFW_PRESS)
			glm_vec3_scale(state.world.forward.raw, movespeed, state.world.velocity.raw);
		if (glfwGetKey(state.window, GLFW_KEY_S) == GLFW_PRESS || glfwGetKey(state.window, GLFW_KEY_DOWN) == GLFW_PRESS)
			glm_vec3_scale(state.world.forward.raw, -movespeed, state.world.velocity.raw);
		if (glfwGetKey(state.window, GLFW_KEY_D) == GLFW_PRESS || glfwGetKey(state.window, GLFW_KEY_RIGHT) == GLFW_PRESS)
			glm_vec3_scale(state.world.right.raw, movespeed, state.world.velocity.raw);
		if (glfwGetKey(state.window, GLFW_KEY_A) == GLFW_PRESS || glfwGetKey(state.window, GLFW_KEY_LEFT) == GLFW_PRESS)
			glm_vec3_scale(state.world.right.raw, -movespeed, state.world.velocity.raw);
		state.world.velocity.y = yvel;

		// Perform game logic
		world_logic(&state.world, dt, player_height);
		em_update(&state.em, &state.world, dt, current_time);
	}

	clean_up_program();
}

void clean_up_program()
{
	// Clean up resources
	glfwDestroyWindow(state.window);
	glfwTerminate();
}
