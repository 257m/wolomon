#ifndef ENTITY_H
#define ENTITY_H

#include "model.h"
#include "utils/arena.h"

typedef struct {
	vec3s p;  // Position
	vec3s v;  // Velocity
	vec3s a;  // Acceleration
	vec3s dv; // Desired Velocity
	vec3s look; // Direction
	f64 last_update; // Last time it got updated
	u32 m; // Index of model in models array
	u32 tag;
	// Later it will have a forward and right vector for direction
} Entity;

void entity_render(Entity* e, World* w);
void entity_update(Entity* e, World* w, f64 dt);

// hehe indexed ring buffer shenanigans
typedef struct {
	Entity* e; // Entities
	u32* ids;
	size head, tail;
	size amount;
	ssize allocated;
	size max;
} Entity_Manager;

void em_init(Entity_Manager* em, size n, size max);
void em_add_entity(Entity_Manager* em, World* w);
void em_delete_entity(Entity_Manager* em, u32 id);
void em_update(Entity_Manager* em, World* w, f64 dt, f64 t);
void em_render(Entity_Manager* em, World* w);

#endif /* ENTITY_H */
