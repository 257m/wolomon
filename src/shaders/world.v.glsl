#version 420

in float altitude;
in uvec2 coord;
uniform mat4 mvp;
out float taltitude;

void main(void) {
        if (altitude >= 0)
                gl_Position = mvp * vec4(coord.x, altitude, coord.y, 1.0);
        else
                gl_Position = mvp * vec4(coord.x, 0, coord.y, 1.0);
        taltitude = altitude;
}
