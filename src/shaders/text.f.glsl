#version 420

layout(location = 0) out vec4 color;

in vec2 vtexcoord;
uniform vec3 text_colour;
uniform sampler2D atlas;

void main(void)
{
	vec4 temp = texture(atlas, vtexcoord);
	color = vec4(text_colour, temp.a);
}
