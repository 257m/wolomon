#version 420
layout(location = 0) out vec4 color;

in float taltitude;

void main(void) {
        if (taltitude >= 0) {
                float step = (taltitude*taltitude + 2048) / (taltitude*taltitude + 8192);
                color = vec4(step / 4, step / 2, step / 4, 1);
        }
        else {
                float step = 1 - (taltitude*taltitude + 2048) / (taltitude*taltitude + 8192);
                color = vec4(step / 4, step / 4, step / 2, 1);
        }
}
