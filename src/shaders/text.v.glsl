#version 420

in vec4 vertex;
out vec2 vtexcoord;
uniform vec3 text_colour;

void main(void) {
        gl_Position = vec4(vertex.xy, 0.0, 1.0);
        vtexcoord = vertex.zw;
}
