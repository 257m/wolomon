#version 420

layout(location = 0) out vec4 color;

in vec2 vtexcoord;
in vec3 vnormal;
uniform int textureIndex;
uniform sampler2DArray textureArray;

void main(void) {
        color = texture(textureArray, vec3(vtexcoord, textureIndex));
}
