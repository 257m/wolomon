#version 420

in vec3 vertex;
in vec2 texcoord;
in vec3 normal;
out vec2 vtexcoord;
out vec3 vnormal;
uniform mat4 mvp;

void main(void) {
        gl_Position = mvp * vec4(vertex.xyz, 1);
        vtexcoord = texcoord;
        vnormal = normal;
}
