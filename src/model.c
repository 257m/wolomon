#include <stdio.h>
#include <stdlib.h>
#include "model.h"
#include "utils/errors.h"
#include "utils/shader_utils.h"
#include "world.h"
#include "texture.h"
#include "stb/stb_image.h"

GLuint model_program;
static GLint attribute_vertex;
static GLint attribute_texcoord;
static GLint attribute_normal;
static GLint uniform_textureIndex;
GLint uniform_model_mvp;

void model_init()
{
	model_program = shader_create_program("src/shaders/model.v.glsl", "src/shaders/model.f.glsl");

	attribute_vertex = glGetAttribLocation(model_program, "vertex");
  	glVertexAttribPointer(attribute_vertex, 3, GL_FLOAT, GL_FALSE, 0, 0);
  	glEnableVertexAttribArray(attribute_vertex);
	attribute_texcoord = glGetAttribLocation(model_program, "texcoord");
  	glVertexAttribPointer(attribute_texcoord, 2, GL_FLOAT, GL_FALSE, 0, sizeof(f32)*3);
  	glEnableVertexAttribArray(attribute_texcoord);
	attribute_normal = glGetAttribLocation(model_program, "normal");
  	glVertexAttribPointer(attribute_normal, 3, GL_FLOAT, GL_FALSE, 0, sizeof(f32)*5);
  	glEnableVertexAttribArray(attribute_normal);
	uniform_model_mvp = glGetUniformLocation(model_program, "mvp");
	uniform_textureIndex = glGetUniformLocation(model_program, "textureIndex");
	/*PANIC(attribute_vertex == -1 || attribute_texcoord == -1 || attribute_normal == -1 || uniform_model_mvp == -1,
	      "Could not get location of attributes/uniform: %d,%d,%d,%d!\r\n",
	      attribute_vertex, attribute_texcoord, attribute_normal, uniform_model_mvp);*/
	stbi_set_flip_vertically_on_load(true);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // Use GL_NEAREST_MIPMAP_LINEAR if you want to use mipmaps
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

void model_load(Model* m, char* file_str)
{
	FILE* f = fopen(file_str, "r");
	PANIC(!f, "Failed to open model file %s\r\n", file_str);
	// Who cares about the header?
	// Nobody cares about whether the file is actually the type because I can
	// validate ahead of time
	// Skip the 80 byte header
	fseek(f, 80, SEEK_SET);
	fread(m->amounts, sizeof(*m->amounts), 3, f);
	m->vertices = malloc(*m->amounts*sizeof(f32)*8);
	fread(m->vertices, sizeof(f32)*8, *m->amounts, f);
	fclose(f);
	for (u32 i = 0; i < *m->amounts; i++) {
		m->vertices[i*8 + 3] *= (float)textures[m->tex].width/2048;
		m->vertices[i*8 + 4] *= (float)textures[m->tex].height/2048;
	}
	glGenBuffers(1, &m->vbo);
	glBindBuffer(GL_ARRAY_BUFFER, m->vbo);
	glBufferData(GL_ARRAY_BUFFER, *m->amounts*sizeof(f32)*8, m->vertices, GL_STATIC_DRAW);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
}

void model_render(World* w, Model* m)
{
	glBindBuffer(GL_ARRAY_BUFFER, m->vbo);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D_ARRAY, tex_array);
	glUniform1i(uniform_textureIndex, m->tex);
  	glVertexAttribPointer(attribute_vertex, 3, GL_FLOAT, GL_FALSE, sizeof(f32)*8, 0);
  	glVertexAttribPointer(attribute_texcoord, 2, GL_FLOAT, GL_FALSE, sizeof(f32)*8, sizeof(f32)*3);
  	glVertexAttribPointer(attribute_normal, 3, GL_FLOAT, GL_FALSE, sizeof(f32)*8, sizeof(f32)*5);
  	glDrawArrays(GL_TRIANGLES, 0, *m->amounts);
  	glBindBuffer(GL_ARRAY_BUFFER, 0);
}
