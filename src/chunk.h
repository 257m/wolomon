#ifndef CHUNK_H
#define CHUNK_H

#include <GL/gl.h>
#include "cglm/struct/vec2.h"
#include "utils/data_types.h"
#include "uthash.h"

#define CHUNK_SIZE_X 32
#define CHUNK_SIZE_Z 32
#define CHUNK_SIZE_FX 32.0f
#define CHUNK_SIZE_FZ 32.0f
#define CHUNK_SIZE (CHUNK_SIZE_X * CHUNK_SIZE_Z)

// World height for optimizations
#define CHUNK_SIZE_FY 512.0f

typedef struct {
	vec3s p;
	u32 m;
} Chunk_Structure;

typedef struct {
	ivec2s id;
	GLuint vbo;
	Chunk_Structure* structs;
	u32 s; // Amount of structures in chunk
	u32 a; // Amount allocated
	UT_hash_handle hh;
} Chunk;

struct World_t;
typedef struct World_t World;

Chunk* chunk_create(World* w, ivec2s pos);
void chunk_generate(Chunk* c, World* w);
void chunk_render(Chunk* c, World* w);

#define gnoise(x, z) 64*pnoise2d((x)/8, (z)/8, 1.5f, 5, w->seed.height_map)

#endif /* CHUNK_H */
