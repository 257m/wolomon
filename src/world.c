#include <glad/gl.h>
#include "cglm/mat4.h"
#include "cglm/vec3.h"
#include "state.h"
#include "world.h"
#include "chunk.h"
#include "utils/errors.h"
#include "utils/shader_utils.h"
#include "cglm/cglm.h"
#include "perlin.h"

static GLuint program;
GLint attribute_altitude;
GLint attribute_coord;
GLint uniform_mvp;

void world_vector_update(World* w, f64 dx, f64 dy)
{
	w->forward.x = sinf(w->angle.x);
	w->forward.y = 0;
	w->forward.z = cosf(w->angle.x);

	w->right.x = -cosf(w->angle.x);
	w->right.y = 0;
	w->right.z = sinf(w->angle.x);

	w->lookat.x = sinf(w->angle.x) * cosf(w->angle.y);
	w->lookat.y = sinf(w->angle.y);
	w->lookat.z = cosf(w->angle.x) * cosf(w->angle.y);

	glm_cross(w->right.raw, w->lookat.raw, w->up.raw);
}

void world_seed_generate(World_Seed *s)
{
	s->height_map = rand();	
	s->heat_map = rand();	
	s->humidity_map = rand();	
	s->structure_map = rand();	
}

void world_init(World* w, u8 rend_dist)
{
	world_seed_generate(&w->seed);
	w->rend_dist = rend_dist;
	program = shader_create_program("src/shaders/world.v.glsl", "src/shaders/world.f.glsl");

	attribute_altitude = glGetAttribLocation(program, "altitude");
	attribute_coord = glGetAttribLocation(program, "coord");
	uniform_mvp = glGetUniformLocation(program, "mvp");
	PANIC(attribute_altitude == -1 || attribute_coord == -1 || uniform_mvp == -1, "Could not get location of attribute coord and or uniform mvp!\r\n");

	glUseProgram(program);
	glClearColor(0.6, 0.8, 1.0, 0.0);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
	//glEnable(GL_POLYGON_OFFSET_FILL);
	//glPolygonOffset(1, 1);

	glEnableVertexAttribArray(attribute_coord);
	glm_vec3_copy(vec3(0, 32, 0), w->position.raw);
	glm_vec3_copy(vec3(1, 0, 0), w->angle.raw);
	glm_vec3_copy(vec3(0, 1, 0), w->up.raw);
	glm_perspective(45.0f, (float)state.window_width/state.window_height, 0.1f, 10000.0f, w->projection);
	world_vector_update(&state.world, 0, 0);
	
	w->chunks = NULL;

}

void world_logic(World* w, f64 dt, f32 player_height)
{
	f32 ground = gnoise(w->position.x/CHUNK_SIZE_FX, w->position.z/CHUNK_SIZE_FZ); 
	vec3 acceleration = {0, 0, 0};
	acceleration[0] = -w->velocity.x*0.1;
	acceleration[2] = -w->velocity.z*0.1;
	if (w->position.y > ground + player_height)
		acceleration[1] = -0.5;
	glm_vec3_add(w->velocity.raw, acceleration, w->velocity.raw);
	if (w->velocity.y < -50)
		w->velocity.y = -50;
	vec3 delta_velocity;
	glm_vec3_scale(w->velocity.raw, dt, delta_velocity);
	glm_vec3_add(w->position.raw, delta_velocity, w->position.raw);
	if (w->position.y < ground + player_height)
		w->position.y = ground + player_height;
}

void world_render(World* w)
{
	mat4 view;
	vec3 center;
	glm_vec3_add(w->position.raw, w->lookat.raw, center);
	glm_lookat(w->position.raw, center, w->up.raw, view);

	mat4 mvp;
	glm_mat4_mul(w->projection, view, w->pv);
	ivec2s pos;
	u32 chunks_loaded = 0;
	for (pos.y = floor(w->position.z / CHUNK_SIZE_Z) - w->rend_dist; pos.y <= floor(w->position.z / CHUNK_SIZE_Z) + w->rend_dist; pos.y++)
		for (pos.x = floor(w->position.x / CHUNK_SIZE_X) - w->rend_dist; pos.x <= floor(w->position.x / CHUNK_SIZE_X) + w->rend_dist; pos.x++) {
			mat4 model = GLM_MAT4_IDENTITY_INIT;
			glm_translate(model, vec3(pos.x*CHUNK_SIZE_FX, 0.0f, pos.y*CHUNK_SIZE_FZ));
			glm_mat4_mul(w->pv, model, mvp);
			
			// Is this chunk on the screen?
			vec4s chunk_center;
			glm_mat4_mulv(mvp, vec4(CHUNK_SIZE_FX / 2, CHUNK_SIZE_FY / 2, CHUNK_SIZE_FZ / 2, 1), chunk_center.raw);

			chunk_center.x /= chunk_center.w;
			chunk_center.y /= chunk_center.w;

			// If it is behind the camera, don't bother drawing it
			if (chunk_center.z < -CHUNK_SIZE_FY / 2)
				continue;

			// If it is outside the screen, don't bother drawing it
			//if (fabsf(chunk_center.x) > 1 + fabsf(CHUNK_SIZE_FY * 2 / chunk_center.w) || fabsf(chunk_center.y) > 1 + fabsf(CHUNK_SIZE_FY * 2 / chunk_center.w))
			//	continue;

			Chunk *c;
			HASH_FIND(hh, w->chunks, pos.raw, sizeof(ivec2s), c);
			if (!c) {
				if (chunks_loaded++ > 8)
					continue;
				c = chunk_create(w, pos);
			}
			glUseProgram(program);
			glUniformMatrix4fv(uniform_mvp, 1, GL_FALSE, mvp);
			chunk_render(c, w);
		}
}
