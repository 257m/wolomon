#include <glad/gl.h>
#include "chunk.h"
#include "uthash.h"
#include "world.h"
#include "perlin.h"
#include "model.h"
#include "cglm/affine.h"

Chunk* chunk_create(World* w, ivec2s pos)
{
	Chunk* c = malloc(sizeof(Chunk));
	memset(c, 0, sizeof(Chunk));
	c->id = pos;
	HASH_ADD(hh, w->chunks, id, sizeof(ivec2s), c);
	chunk_generate(c, w);
	return c;
}

void generate_structure(Chunk* c, vec3s p, u32 m)
{
	if (c->s >= c->a) {
		c->a *= 2;
		c->structs = realloc(c->structs, c->a*sizeof(Chunk_Structure));
	}
	c->structs[c->s].p.x = c->id.raw[0]*CHUNK_SIZE_FX + p.x;
	c->structs[c->s].p.y = p.y;
	c->structs[c->s].p.z = c->id.raw[1]*CHUNK_SIZE_FZ + p.z;
	c->structs[c->s].m = m;
	c->s++;
}

void chunk_generate_structures(Chunk *c, World *w, vec3s p)
{
	f32 h = pnoise2d(c->id.raw[0] + ((p.x)/CHUNK_SIZE_FX), c->id.raw[1] + ((p.z)/CHUNK_SIZE_FZ), 0.5f, 3, w->seed.structure_map);
	// Gonna have to refactor this a lot later. Might even use a different type of noise.
	if (h > 0.9 && h < 1.2 && floor((h*100-(long)h*100)*100) < 5 && p.y > 0)
		generate_structure(c, p, MODEL_TREE);	
	else if ((long)(h*100000) % 10000 == 0)
		generate_structure(c, p, (*((int*)&h) & 0b1) ? MODEL_ROCK1 : MODEL_ROCK2);
}

typedef struct {
	f32 y;
	u8 x, z;
} __attribute__ ((packed)) Vert;

void chunk_generate(Chunk* c, World* w)
{
	glGenBuffers(1, &c->vbo);
	#define noise(x, z) gnoise(c->id.raw[0] + ((x)/CHUNK_SIZE_FX), c->id.raw[1] + ((z)/CHUNK_SIZE_FZ))
	#define vert(x, z) (Vert){noise(x, z), x, z};
	Vert vertex [2*(CHUNK_SIZE+2*CHUNK_SIZE_Z)];
	u32 i = 0;
	c->s = 0;
	c->a = 1;
	c->structs = malloc(sizeof(Chunk_Structure));
	for (u8 z = 0; z < CHUNK_SIZE_Z; z++) {
		for (u8 x = 0; x < CHUNK_SIZE_X; x++) {
			vertex[i++] = vert(x, z);
			vertex[i++] = vert(x, z+1);

			chunk_generate_structures(c, w, (vec3s){x, vertex[i-2].y, z});
		}
		// Last line segment
		vertex[i++] = vert(CHUNK_SIZE_X, z);
		vertex[i++] = vert(CHUNK_SIZE_X, z+1);
		// Degenerates....
		vertex[i++] = vert(CHUNK_SIZE_X, z+1);
		vertex[i++] = vert(0, z+1);
	}
	glBindBuffer(GL_ARRAY_BUFFER, c->vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertex, vertex, GL_STATIC_DRAW);
}

void chunk_render(Chunk* c, World* w)
{
	// Render terrain mesh
	glBindBuffer(GL_ARRAY_BUFFER, c->vbo);
	glVertexAttribPointer(attribute_altitude, 1, GL_FLOAT, GL_FALSE, sizeof(Vert), 0); // for y
	glVertexAttribIPointer(attribute_coord, 2, GL_BYTE, sizeof(Vert), (void*)sizeof(f32)); // for x and z
	glDrawArrays(GL_TRIANGLE_STRIP, 0, 2*(CHUNK_SIZE+2*CHUNK_SIZE_Z));

	// Render models
	glUseProgram(model_program);
	for (u32 i = 0; i < c->s; i++) {
		mat4 mvp;
		mat4 model = GLM_MAT4_IDENTITY_INIT;
		glm_translate(model, c->structs[i].p.raw);
		glm_mat4_mul(w->pv, model, mvp);
		glUniformMatrix4fv(uniform_model_mvp, 1, GL_FALSE, mvp);
		model_render(w, models + c->structs[i].m);
	}
}
