#ifndef PERLIN_H
#define PERLIN_H

#include "utils/data_types.h"

typedef f32 pfloat;

extern pfloat rawnoise(i32 n);

extern pfloat noise1d(i32 x, i32 octave, i32 seed);

extern pfloat noise2d(i32 x, i32 y, i32 octave, i32 seed);

extern pfloat noise3d(i32 x, i32 y, i32 z, i32 octave, i32 seed);

extern pfloat interpolate(pfloat a, pfloat b, pfloat t);

extern pfloat smooth1d(pfloat x, i32 octave, i32 seed);

extern pfloat smooth2d(pfloat x, pfloat y, i32 octave, i32 seed);

extern pfloat smooth3d(pfloat x, pfloat y, pfloat z, i32 octave, i32 seed);

extern pfloat pnoise1d(pfloat x, pfloat persistence, i32 octaves, i32 seed);

extern pfloat pnoise2d(pfloat x, pfloat y, pfloat persistence, i32 octaves, i32 seed);

extern pfloat pnoise3d(pfloat x, pfloat y, pfloat z, pfloat persistence, i32 octaves, i32 seed);

extern pfloat normalize1d(pfloat input);

extern pfloat normalize_max(pfloat input, pfloat max);

extern pfloat normalize_based_max(pfloat input, pfloat base, pfloat max);

extern pfloat sigmoid(pfloat input);

extern pfloat relu(pfloat input);

#endif /* PERLIN_H */
