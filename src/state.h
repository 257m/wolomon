#ifndef STATE_H
#define STATE_H

#include "entity.h"
#include "utils/data_types.h"
#include "event_queue.h"
#include "world.h"

typedef struct {
	World world;
	Entity_Manager em;
	GLFWwindow* window;
	u32 window_width, window_height;
	Event_Queue* event_queue;
    f64 xpos, ypos;
} State;

extern State state;

#endif /* STATE_H */
