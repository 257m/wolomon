#ifndef ARENA_H
#define ARENA_H

#include "data_types.h"
#include <stddef.h>

typedef struct {
	void* data;
	size n;
	size size;
	u8 flags;
} Arena;

#define Arena(size) (Arena){malloc(size), 0, size};
#define Arena_dynamic(size) (Arena){malloc(size), 0, size, ARENA_DYNAMIC};

enum Arena_Flags {
	ARENA_DYNAMIC = 0b1,
};

void* arena_alloc(Arena* a, size n);
size_t arena_alloc_index(Arena* a, size n);
#define arena_check(a, amount) (a->n + amount < a->size)
#define arena_reset(a) a->n = 0; 

#endif /* ARENA_H */
