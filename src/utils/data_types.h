#ifndef DATA_TYPES_H
#define DATA_TYPES_H

#include <stdint.h>
#include <unistd.h>

#define i8(x) (i8){x}
typedef int8_t i8;
#define i16(x) (i16){x}
typedef int16_t i16;
#define i32(x) (i32){x}
typedef int32_t i32;
#define i64(x) (i64){x}
typedef int64_t i64;

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;

typedef size_t size;
typedef ssize_t ssize;

typedef float f32;
typedef double f64;

// Some missing types from cglm
typedef unsigned int uvec2 [2];
typedef u8 u8vec2 [2];

typedef u8 u8vec3 [3];
typedef union {
	u8vec3 raw;
	struct {
		u8 x, y, z;
	};
} u8vec3s;

typedef u8 u8vec4 [4];
typedef union {
	u8vec4 raw;
	struct {
		u8 x, y, z, w;
	};
} u8vec4s;

#define member(type, member) ((type *)0)->member
#define member_size(type, member) sizeof(((type *)0)->member)
#define array_size(a) (sizeof a / sizeof *a)
#define create(type, ...) (type){__VA_ARGS__}

#endif /* DATA_TYPES_H */
