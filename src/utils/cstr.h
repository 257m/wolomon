#ifndef CSTR_H
#define CSTR_H

#include <stdlib.h>
#include <stdbool.h>
#include "arena.h"

typedef struct {
	char* data;
	size len;
	size allocated;
} string;

#define string(x) (string){x, sizeof(x)-1, sizeof(x)}
#define string_size(size) (string){alloca(size), 0, size}
#define string_form(x) x.len, x.data
#define string_null (string){0, 0, 0}

string str_init(Arena* a, size len);
string str_concat(Arena* a, string s1, string s2);
string str_substring(string s, size start, size end);
string str_substring_new(string s, size start, size end, Arena* a);
bool str_compare(string s1, string s2);
size str_split(string s, string delim, Arena* a, string** arr);
string str_get_line(string s);
string str_until(string s, string delim, string* after);
void cstr_copy(string s, char* c);
void str_dehtml(string in, string* out);

#endif /* CSTR_H */
