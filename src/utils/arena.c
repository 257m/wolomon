#include "arena.h"
#include <stdlib.h>

void* arena_alloc(Arena* a, size n)
{
	void* b = a->data + a->n;
	a->n += n;
	if (a->n >= a->size) {
		if (a->flags & ARENA_DYNAMIC) {
			a->size *= 2;
			a->data = realloc(a->data, a->size);
			return (a->data + a->n - n);
		}
		return 0;
	}
	return b;
}

size_t arena_alloc_index(Arena* a, size n)
{
	size_t b = a->n;
	a->n += n;
	if (a->n >= a->size) {
		if (a->flags & ARENA_DYNAMIC) {
			a->size *= 2;
			a->data = realloc(a->data, a->size);
			return a->n;
		}
		return 0;
	}
	return b;
}
