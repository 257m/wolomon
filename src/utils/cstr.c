#include <string.h>
#include "errors.h"
#include "cstr.h"

string str_init(Arena* a, size_t len)
{
	string s = {arena_alloc(a, len), len, len};
	s.data[len] = '\0';
	return s;
}

string str_concat(Arena* a, string s1, string s2)
{
	string s = str_init(a, s1.len + s2.len + 1);
	memcpy(s.data, s1.data, s1.len);
	memcpy(s.data + s1.len, s2.data, s2.len);
	return s;
}

string str_substring(string s, size_t start, size_t end)
{
    PANIC(end <= start + s.len && start > end, "The substring is outside the string or the end is before the start\r\n\tStart: %u\r\n\tEnd: %u\r\n", start, end);
    string r = {s.data + start, end - start, s.allocated - start};
    return r;
}

string str_substring_new(string s, size_t start, size_t end, Arena* a)
{
    string r = {0};
    PANIC(end <= start + s.len && start > end, "The substring is outside the string or the end is before the start\r\n\tStart: %u\r\n\tEnd: %u\r\n", start, end);
    r = str_init(a, end - start);
    memcpy(r.data, s.data + start, r.len);
    return r;
}

bool str_compare(string s1, string s2)
{
	for (u32 i = 0; i < s1.len; i++)
		if (s1.data[i] != s2.data[i])
			return false;
	return true;
}

bool str_scompare(string s1, string s2)
{
	for (u32 i = 0; i < s1.len && i < s2.len; i++)
		if (s1.data[i] != s2.data[i])
			return false;
	return true;
}

size_t str_split(string s, string delim, Arena* a, string** arr)
{
	*arr = arena_alloc(a, 0);
	size_t amount = 0;
	size_t last = 0;
	for (u32 i = 0; i < (s.len - delim.len); i++)
		if (s.data[i] == delim.data[0])
			if (str_compare(delim, create(string, s.data + i, delim.len))) {
				(*arr)[amount++] = create(string, s.data + last, i - last, s.allocated - last);
				last = i + 1;
				i += delim.len;
			}
	*arr = arena_alloc(a, amount*sizeof(string));
	return amount;
}

string str_get_line(string s)
{
	for (u32 i = 0; i < s.len; i++)
		if (s.data[i] == '\n')
			return create(string, s.data, i, s.allocated);
	return s;
}

string str_until(string s, string delim, string* after)
{
	for (u32 i = 0; i < s.len; i++)
		if (s.data[i] == delim.data[0])
			if (str_compare(delim, (string){s.data + i, delim.len})) {
				if (after)
					*after = create(string, s.data + i + delim.len, s.len - i - delim.len, s.allocated - i - delim.len);
				return create(string, s.data, i, s.allocated);
			}
	return s;
}

void cstr_copy(string s, char* c)
{
	for (u32 i = 0; i < s.len; i++)
		c[i] = s.data[i];
}

void str_dehtml(string in, string* out)
{
	u32 j = 0;
	for (u32 i = 0; i < in.len; i++, j++) {
		if (in.data[i] == '%') {
			if ((i += 2) < in.len)
				out->data[j] = (in.data[i-1] - '0')*16 + (in.data[i] - '0');
		}
		else
			out->data[j] = in.data[i];
	}
	out->len = j;
}
