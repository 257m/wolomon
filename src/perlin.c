#include <math.h>
#include "utils/data_types.h"
#include "perlin.h"

/*
 * Shamelessly stolen from czinn.
 * I didn't use my perlin version because I didn't know how to do octaves and persistence
 * Credit: https://github.com/czinn/perlin/tree/master
*/

pfloat rawnoise(i32 n)
{
    n = (n << 13) ^ n;
    return (1.0 - ((n * (n * n * 15731 + 789221) + 1376312589) & 0x7fffffff) / 1073741824.0);
}

pfloat noise1d(i32 x, i32 octave, i32 seed)
{
    return rawnoise(x * 1619 + octave * 3463 + seed * 13397);
}

pfloat noise2d(i32 x, i32 y, i32 octave, i32 seed)
{
    return rawnoise(x * 1619 + y * 31337 + octave * 3463 + seed * 13397);
}

pfloat noise3d(i32 x, i32 y, i32 z, i32 octave, i32 seed)
{
    return rawnoise(x * 1919 + y * 31337 + z * 7669 + octave * 3463 + seed * 13397);
}

pfloat interpolate(pfloat a, pfloat b, pfloat t)
{
    // Smooth interpolate
    // pfloat f = (1 - cos(t * 3.141593)) * 0.5;
    // return a * (1 - f) + b * f;

    // Smooth Step
    return (b - a) * (3.0 - t * 2.0) * t * t + a;

    // Fast interpolate
    // return (b - a) * t + a;
}

pfloat smooth1d(pfloat x, i32 octave, i32 seed)
{
    i32 int_x = floor(x);
    pfloat fracx = x - int_x;

    pfloat v1 = noise1d(int_x, octave, seed);
    pfloat v2 = noise1d(int_x + 1, octave, seed);

    return interpolate(v1, v2, fracx);
}

pfloat smooth2d(pfloat x, pfloat y, i32 octave, i32 seed)
{
    // Casting to i32 was being weird
    i32 int_x = floor(x);
    pfloat fracx = x - int_x;
    i32 int_y = floor(y);
    pfloat fracy = y - int_y;
    
    pfloat v1 = noise2d(int_x, int_y, octave, seed);
    pfloat v2 = noise2d(int_x + 1, int_y, octave, seed);
    pfloat v3 = noise2d(int_x, int_y + 1, octave, seed);
    pfloat v4 = noise2d(int_x + 1, int_y + 1, octave, seed);
    
    pfloat i1 = interpolate(v1, v2, fracx);
    pfloat i2 = interpolate(v3, v4, fracx);
    
    return interpolate(i1, i2, fracy);
}

pfloat smooth3d(pfloat x, pfloat y, pfloat z, i32 octave, i32 seed)
{
    i32 int_x = floor(x);
    pfloat fracx = x - int_x;
    i32 int_y = floor(y);
    pfloat fracy = y - int_y;
    i32 int_z = floor(z);
    pfloat fracz = z - int_z;


    pfloat v1 = noise3d(int_x, int_y, int_z, octave, seed);
    pfloat v2 = noise3d(int_x + 1, int_y, int_z, octave, seed);
    pfloat v3 = noise3d(int_x, int_y + 1, int_z, octave, seed);
    pfloat v4 = noise3d(int_x + 1, int_y + 1, int_z, octave, seed);
    pfloat v5 = noise3d(int_x, int_y, int_z + 1, octave, seed);
    pfloat v6 = noise3d(int_x + 1, int_y, int_z + 1, octave, seed);
    pfloat v7 = noise3d(int_x, int_y + 1, int_z + 1, octave, seed);
    pfloat v8 = noise3d(int_x + 1, int_y + 1, int_z + 1, octave, seed);

    pfloat i1 = interpolate(v1, v2, fracx);
    pfloat i2 = interpolate(v3, v4, fracx);
    pfloat i3 = interpolate(v5, v6, fracx);
    pfloat i4 = interpolate(v7, v8, fracx);
    
    pfloat j1 = interpolate(i1, i2, fracy);
    pfloat j2 = interpolate(i3, i4, fracy);

    return interpolate(j1, j2, fracz);
}

pfloat pnoise1d(pfloat x, pfloat persistence, i32 octaves, i32 seed)
{
    pfloat total = 0.0;
    pfloat frequency = 1.0;
    pfloat amplitude = 1.0;
    i32 i = 0;

   for (i = 0; i < octaves; i++) {
       total += smooth1d(x * frequency, i, seed) * amplitude;
       frequency /= 2;
       amplitude *= persistence;
   } 

   return total;
}

pfloat pnoise2d(pfloat x, pfloat y, pfloat persistence, i32 octaves, i32 seed)
{
   pfloat total = 0.0;
   pfloat frequency = 1.0;
   pfloat amplitude = 1.0;
   i32 i = 0;
   
   for (i = 0; i < octaves; i++) {
       total += smooth2d(x * frequency, y * frequency, i, seed) * amplitude;
       frequency /= 2;
       amplitude *= persistence;
   } 

   return total;
}

pfloat pnoise3d(pfloat x, pfloat y, pfloat z, pfloat persistence, i32 octaves, i32 seed)
{
   pfloat total = 0.0;
   pfloat frequency = 1.0;
   pfloat amplitude = 1.0;
   i32 i = 0;
   
   for (i = 0; i < octaves; i++) {
       total += smooth3d(x * frequency, y * frequency, z * frequency, i, seed) * amplitude;
       frequency /= 2;
       amplitude *= persistence;
   } 

   return total;
}

// Helper function for normalization of perlin noise
pfloat normalize1d(pfloat input)
{
    return (input*input) / (1 + input*input);
}

pfloat normalize_max(pfloat input, pfloat max)
{
    return (input*input) / (input*input + max);
}

pfloat normalize_based_max(pfloat input, pfloat base, pfloat max)
{
    return (input*input + base) / (input*input + max);
}

pfloat sigmoid(pfloat input)
{
    return 1.0 / (1 + exp(-input));
}

pfloat relu(pfloat input)
{
    if (input > 0)
        return input;
    return 0;
}
