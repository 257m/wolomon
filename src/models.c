#include "model.h"
#include "stb/stb_image.h"

GLuint tex_array;

Model models[MODEL_MAX] = {
	{"models/blujie.s3m", TEXTURE_TEX},
	{"models/tree.s3m", TEXTURE_TEX},
	{"models/rock1.s3m", TEXTURE_STONE},
	{"models/rock2.s3m", TEXTURE_STONE},
};

Texture textures[TEXTURE_MAX] = {
	{"images/lowpoly.png"},
	{"images/stone.jpg"}
};

void models_init() {
	// Generate texture array object
	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &tex_array);
	glBindTexture(GL_TEXTURE_2D_ARRAY, tex_array);

	// Allocate storage for the texture array
	glTexStorage3D(GL_TEXTURE_2D_ARRAY, 1, GL_RGBA8, 2048, 2048, TEXTURE_MAX);

	for (u32 i = 0; i < TEXTURE_MAX; i++) {
		// Upload texture data for the ith layer
		textures[i].data = stbi_load(textures[i].name, &textures[i].width, &textures[i].height, &textures[i].id, 4);
		glTexSubImage3D(GL_TEXTURE_2D_ARRAY, 0, 0, 0, i, textures[i].width, textures[i].height, 1,
                    GL_RGBA, GL_UNSIGNED_BYTE, textures[i].data);
        stbi_image_free(textures[i].data);
	}

	for (u32 i = 0; i < MODEL_MAX; i++)
		model_load(models + i, models[i].file_name);
}
