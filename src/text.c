#include <glad/gl.h>
#include "cglm/types-struct.h"
#include "utils/data_types.h"
#include "utils/cstr.h"
#include "utils/shader_utils.h"
#include "utils/errors.h"
#include "texture.h"
#include "stb/stb_image.h"
#include "cglm/struct.h"

Texture font = {"fonts/font_atlas.png"};
GLuint text_vbo;
static GLuint text_program;
static GLint attribute_vertex;
static GLint uniform_text_colour;

void text_init()
{
	font.data = stbi_load(font.name, &font.width, &font.height, &font.id, 4);
	PANIC(!font.data, "Image did not load!\r\n");

	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &font.id);
	glBindTexture(GL_TEXTURE_2D, font.id);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, font.width, font.height, 0,
		GL_RGBA, GL_UNSIGNED_BYTE, font.data);

	glGenBuffers(1, &text_vbo);

	text_program = shader_create_program("src/shaders/text.v.glsl", "src/shaders/text.f.glsl");

	attribute_vertex = glGetAttribLocation(text_program, "vertex");
	glVertexAttribPointer(attribute_vertex, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(attribute_vertex);
	uniform_text_colour = glGetUniformLocation(text_program, "text_colour");

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST); // Use GL_NEAREST_MIPMAP_LINEAR if you want to use mipmaps
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
}

static f32 text_width = 0.03125f, text_height = 0.0625f;

void text_set_size(f32 width, f32 height)
{
	text_width = width;
	text_height = height;
}

void text_render(string str, f32 x, f32 y, vec3s c)
{
	vec4s vertex [6*str.len];
	u32 e = 0;

	for (u32 i = 0; i < str.len; i++) {
		u32 tex_x = (str.data[i]) & 0x0F;
		u32 tex_y = 7 - (str.data[i] >> 4);
		vertex[e++] = vec4s(x + i*text_width,              y,               tex_x*0.0625f,           tex_y*0.125f);
		vertex[e++] = vec4s(x + i*text_width,              y + text_height, tex_x*0.0625f,           tex_y*0.125f + 0.125f);	
		vertex[e++] = vec4s(x + i*text_width + text_width, y,               tex_x*0.0625f + 0.0625f, tex_y*0.125f);
		vertex[e++] = vec4s(x + i*text_width + text_width, y,               tex_x*0.0625f + 0.0625f, tex_y*0.125f);
		vertex[e++] = vec4s(x + i*text_width,              y + text_height, tex_x*0.0625f,           tex_y*0.125f + 0.125f);	
		vertex[e++] = vec4s(x + i*text_width + text_width, y + text_height, tex_x*0.0625f + 0.0625f, tex_y*0.125f + 0.125f);	
	}

	glBindBuffer(GL_ARRAY_BUFFER, text_vbo);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertex, vertex, GL_DYNAMIC_DRAW);

	glUseProgram(text_program);
	glDisable(GL_CULL_FACE);
	glDisable(GL_DEPTH_TEST);
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, font.id);
	glVertexAttribPointer(attribute_vertex, 4, GL_FLOAT, GL_FALSE, 0, 0);
	glUniform3fv(uniform_text_colour, 1, c.raw);
	glDrawArrays(GL_TRIANGLES, 0, 6*str.len);
	glEnable(GL_CULL_FACE);
	glEnable(GL_DEPTH_TEST);
}
