#include "entity.h"
#include "cglm/mat4.h"
#include "chunk.h"
#include "model.h"
#include "perlin.h"
#include "state.h"
#include "utils/errors.h"
#include <limits.h>
#include <math.h>
#include <stdlib.h>
#include "cglm/affine.h"
#include "cglm/quat.h"

double uniform_distribution(double low, double high)
{
	double difference = high - low; // The difference between the two
	int scale = 10000;
	int scaled_difference = (int)(difference * scale);
	return low + ((float)(rand() % scaled_difference) / scale);
}

void em_init(Entity_Manager* em, size n, size max)
{
	em->e = malloc(n*sizeof(*em->e));
	em->ids = malloc(n*sizeof(*em->ids));
	for (u32 i = 0; i < n; i++)
		em->ids[i] = i;
	em->head = 0;
	em->tail = 0;
	em->amount = 0;
	em->allocated = n;
	em->max = max;
}

void em_add_entity(Entity_Manager* em, World* w)
{
	if (em->amount >= em->max)
		return;
	if (em->allocated < 0) {
		// Double the size of our allocation
		em->allocated = -em->allocated;
		em->ids = realloc(em->ids, em->allocated*2*sizeof(*em->ids));
		em->e = realloc(em->e, em->allocated*2*sizeof(*em->e));
		// Copy over ids so the block of ids is contiguous and avoids wrap
		memcpy(em->ids + em->allocated, em->ids, em->tail*sizeof(*em->ids));
		// Save start of new ids
		u32 id = em->allocated;
		em->tail = em->head + em->allocated;
		em->allocated *= 2;
		// Generate new ids
		// fill in start of buffer
		for (u32 i = 0; i < em->head; i++)
			em->ids[i] = id++;
		// fill in end of buffer
		for (u32 i = em->tail; i < em->allocated; i++)
			em->ids[i] = id++;
	}
	// Real shit
	em->e[em->ids[em->tail]] = create(Entity,
			.p = {
				uniform_distribution(-w->rend_dist*32 + w->position.x, w->rend_dist*32 + w->position.x),
				0,
				uniform_distribution(-w->rend_dist*32 + w->position.z, w->rend_dist*32 + w->position.z),
			},
			.m = MODEL_BLUJIE, .tag = em->tail, .last_update = -INFINITY);
	em->e[em->ids[em->tail]].p.y = gnoise(em->e[em->ids[em->tail]].p.x/CHUNK_SIZE_FX, em->e[em->ids[em->tail]].p.z/CHUNK_SIZE_FZ);
	em->tail = ++(em->tail) % em->allocated;
	if (em->head == em->tail)
		em->allocated *= -1;
	em->amount++;
}

void em_delete_entity(Entity_Manager* em, u32 id)
{
	if (em->allocated < 0)
		em->allocated *= -1;
	// Switchedoodle doo while updating the tag of the swap
	u32 temp = em->ids[em->head];
	em->ids[em->head] = id;
	em->ids[em->e[id].tag] = temp;
	em->e[temp].tag = em->e[id].tag;
	em->head = ++(em->head) % em->allocated;
	em->amount--;
}

void em_update(Entity_Manager* em, World* w, f64 dt, f64 t)
{
	u32 i = em->head;
	ssize allocated = em->allocated;
	if (i == em->tail && allocated < 0) {
		i++;
		allocated = -allocated;
	}
	while (1) {
		if (i == allocated)
			i = 0;
		if (i == em->tail)
			break;
		u32 k = em->ids[i];

		if ((t - em->e[k].last_update) >= 5) {
			em->e[k].last_update = t;
			// Throw bird in random direction
			em->e[k].dv.x = uniform_distribution(-50, 50);
			em->e[k].dv.y = uniform_distribution(-10, 50);
			em->e[k].dv.z = uniform_distribution(-50, 50);
			em->e[k].v.x = uniform_distribution(-5, 5);
			em->e[k].v.y = uniform_distribution(-5, 25);
			em->e[k].v.z = uniform_distribution(-5, 5);
		}
		f32 dist = fabs(em->e[k].p.x - w->position.x) + fabs(em->e[k].p.z - w->position.z);
		if (dist > w->rend_dist*32) {
			printf("%f\n", dist);
			em_delete_entity(em, k);
		}
		else
			entity_update(em->e + k, w, dt);
		i++;
	}
}

void em_render(Entity_Manager* em, World* w)
{
	u32 i = em->head;
	ssize allocated = em->allocated;
	if (i == em->tail && allocated < 0) {
		i++;
		allocated = -allocated;
	}
	while (1) {
		if (i == allocated)
			i = 0;
		if (i == em->tail)
			break;
		u32 k = em->ids[i];
		entity_render(em->e + k, w);
		i++;
	}
}

void entity_render(Entity* e, World* w)
{
	mat4 model = GLM_MAT4_IDENTITY_INIT;
	mat4 mvp = GLM_MAT4_IDENTITY_INIT;
	versor q;
	glm_quat_for(e->look.raw, vec3(0, 1, 0), q);
	glm_translate(mvp, e->p.raw);
	glm_quat_rotate(mvp, q, model);
	glm_mat4_mul(w->pv, model, mvp);
	glUseProgram(model_program);
	glUniformMatrix4fv(uniform_model_mvp, 1, GL_FALSE, mvp);
	model_render(w, models + e->m);
}

void entity_update(Entity* e, World* w, f64 dt)
{
	f32 ground = gnoise(e->p.x/CHUNK_SIZE_FX, e->p.z/CHUNK_SIZE_FZ); 
	e->a.x = -e->v.x*0.1 + (e->dv.x-e->v.x)/100;
	e->a.y = (e->dv.y-e->v.y)/100;
	e->a.z = -e->v.z*0.1 + (e->dv.z-e->v.z)/100;
	if (e->p.y > ground)
		e->a.y += -0.5;
	glm_vec3_add(e->v.raw, e->a.raw, e->v.raw);
	if (e->v.y < -50)
		e->v.y = -50;
	vec3 delta_velocity;
	glm_vec3_scale(e->v.raw, dt, delta_velocity);
	glm_vec3_add(e->p.raw, delta_velocity, e->p.raw);
	if (e->p.y < ground)
		e->p.y = ground;
	e->look = vec3s(-e->v.x, -e->v.y, -e->v.z);
	if (e->p.y == ground)
		e->look.y = 0;
	glm_normalize(e->look.raw);
}
