#ifndef MODEL_H
#define MODEL_H

#include "utils/data_types.h"
#include "world.h"
#include "texture.h"

typedef struct {
	char* file_name;
	u32 tex;
	char* texture_name;
	f32* vertices;
	union {
		u32 amounts [3];
		struct {
			u32 vertex_amount, texcoord_amount, normal_count;
		};
	};
	GLuint vbo;
} Model;

void model_init();
void model_load(Model* m, char* file_str);
void model_render(World* w, Model* m);
void models_init();

enum {
	MODEL_BLUJIE,
	MODEL_TREE,
	MODEL_ROCK1,
	MODEL_ROCK2,
	MODEL_MAX,
};

enum {
	TEXTURE_TEX,
	TEXTURE_STONE,
	TEXTURE_MAX,
};

extern Model models [MODEL_MAX];
extern Texture textures [TEXTURE_MAX];
extern GLuint tex_array;
extern GLuint model_program;
extern GLint uniform_model_mvp;

#endif /* MODEL_H */
