#ifndef TEXT_H
#define TEXT_H

#include "cglm/types-struct.h"
#include "utils/cstr.h"

void text_init();
void text_render(string str, f32 x, f32 y, vec3s c);

#endif /* TEXT_H */
