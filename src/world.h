#ifndef WORLD_H
#define WORLD_H

#include <glad/gl.h>
#include <stdbool.h>
#include "cglm/vec3.h"
#include "chunk.h"

typedef struct {
	u32 height_map;
	u32 heat_map;
	u32 humidity_map;
	u32 structure_map;
} World_Seed;

typedef struct World_t {
    World_Seed seed;
    Chunk* chunks;
    mat4 projection;
    mat4 pv;
	vec3s position;
	vec3s velocity;
	vec3s forward;
    vec3s right;
    vec3s up;
    vec3s lookat;
    vec3s angle;
	u8 rend_dist;
} World;

extern void world_init(World* w, u8 rend_dist);
extern void world_render(World* w);
extern void world_logic(World* w, f64 dt, f32 player_height);
extern void world_seed_generate(World_Seed* w);
extern void world_vector_update(World* w, f64 dx, f64 dy);

extern GLint attribute_altitude;
extern GLint attribute_coord;
extern GLint uniform_mvp;

#endif /* WORLD_H */
