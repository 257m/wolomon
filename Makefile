# Makefile for building it from linux for linux
CC = gcc
CFLAGS = -g -Wno-everything `pkg-config --cflags glfw3 glu gl` -Iglad/include
LDFLAGS = `pkg-config --libs glfw3 glu gl` -lm

# This is the name of the executable we are going to create
EXE = main

# This is the default target that is going to be built when we run 'make'
all: clean_exe $(EXE)

# This is the directory where our source will be stored
SRC_DIR = src

# This is the list of all source files
SRC = $(wildcard $(SRC_DIR)/**/*.c) $(wildcard $(SRC_DIR)/*.c) $(wildcard $(SRC_DIR)/**/**/*.c) $(wildcard $(SRC_DIR)/**/**/**/*.c)

# This is the directory where our object files will be stored
OBJ_DIR = obj

# This is the list of object files that we need to create
OBJ = $(patsubst $(SRC_DIR)/%.c, $(OBJ_DIR)/%.o, $(SRC))

# This is the rule to build the executable
$(EXE): $(OBJ) glad/glad.o
		$(CC) $^ -o $@ $(LDFLAGS)

# These are the rules to build the object files
$(OBJ_DIR)/%.o: $(SRC_DIR)/%.c
		mkdir -p $(dir $@)
		$(CC) -c $(CFLAGS) $< -o $@

glad/glad.o: glad/src/gl.c
	$(CC) -c $(CFLAGS) $< -o $@

clean_exe:
	rm -f $(EXE)

clean:
	rm -rf glad/glad.o $(OBJ_DIR) $(EXE)
