# CHANGELOG

## (24-02-28)
- Windows build is done
- Made some balance changes to gravity

## (24-02-27)
- Crude Physics implemented

## (24-02-26)
- Polygonal terrain mesh renderer implemented
